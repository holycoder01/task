# Task

## Description

Exposed service (s1) runs on port 8000.  
GET query `/fetchData` returns a JSON object containing base64 encoded data and an error message.  
The data comes from `/data` endpoint of service (s2) running on port 3000 visible only to the exposed service (s1). It is hex encoded.  
There is also a simple script on `/` (on s1) which assumes the data is a PNG image.
The image is fetched and appended to the body of the page.

```mermaid
graph TD;
s1[s1:8000/fetchData]-->|JSON|client;
s2[s2:3000/data]-->|Hex encoded data|s1;
```

## Running

Run service with: `docker-compose build && docker-compose up`
