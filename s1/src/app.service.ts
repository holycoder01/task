import { Injectable, HttpService } from '@nestjs/common';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class AppService {
  getHello(): string {
    return `<style>
body {
  text-align: center;
  padding: 2em;
}
</style>
<script>
window.onload = ()=>{
  function addImage(data) {
    if (data.error)
      return console.log(data.error)

    let img = document.createElement('img')
    img.src = 'data:image/png;base64,' + data.data
    document.body.appendChild(img)
  }
  fetch('/fetchData').then(
    r => r.json()
  ).then(addImage)
}
</script>
<p>Hello!</p>`;
  }
}

export class DataObject {
  constructor(
    private readonly data: string,
    private readonly error: string
  ) {}
}

@Injectable()
export class DataService {
  constructor(private readonly http: HttpService) {}

  fetchData(): Observable<DataObject> {
    const s2 = `http://s2:3000`;
    const data = this.http.get(s2 + '/data').pipe(
      map(r => r.data),
      map(hex => Buffer.from(hex, 'hex').toString('base64')),
      map(b64 => new DataObject(b64, null)),
      catchError(e => of(new DataObject(null, e.message)))
    );
    return data;
  }
}
