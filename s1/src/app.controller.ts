import { Controller, Get } from '@nestjs/common';
import { AppService, DataService, DataObject } from './app.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly dataService: DataService
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('fetchData')
  fetchData() {
    return this.dataService.fetchData();
  }
}
